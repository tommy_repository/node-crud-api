var rootLibraries = '../libraries/';

var helpFunctions = require(rootLibraries + 'helpFunctions');
var validator = require('validator');

var message = {
	OK: {status:200, message:'Success'},
	ERROR: {status:400, message:'Bad request'},
	NOT_FOUND: {status:404, message:'Not found!'},
	NOT_LOGGED_IN: {status:401, message:'Not logged in!'}
};

var acceptHeaders = [
	{name:'x-pagination-per-page',optionName:'perPage',check:function(value){return validator.isInt(value,{min:1,max:20000});}},
	{name:'x-pagination-current-page',optionName:'page',check:function(value){return validator.isInt(value,{min:1,max:20000});}},
	{name:'x-pagination-total-count',optionName:'page',check:function(value){return validator.isInt(value,{min:0,max:200000});}},
	{name:'x-pagination-page-count',optionName:'page',check:function(value){return validator.isInt(value,{min:1,max:200000});}}
]

var rows = [];
var response = {};
var error = false;
var header = [
	{name:'Content-Type', value:'application/json'}
]

var other = {};

/* Private functions */

var InitVars = function(){
	rows = [];
	response = {};
	error = false;
	header = [
		{name:'Content-Type', value:'application/json'}
	]

	other = {};
}

var createResponse = function (){

	if (!helpFunctions.isArray(rows)){
		response.error = message.ERROR;
		response.data = [];
		if (other.hasOwnProperty('err')){
			response.data = other.err;
		}
			error = true;
		return response;
 	}else {
 		response.data = rows;
 	}

 	if (rows.length > 0) {
			response.error = message.OK;
	}else{
		response.error = message.NOT_FOUND;
		return response;
	}

	return response;
}

var createHeaders = function (){
	var hasTotal = false;
	if (rows.length > 0){
		if (rows[0].hasOwnProperty('total_row_count')) {
			hasTotal = true;
			header.push({name:acceptHeaders[2].name, value:(acceptHeaders[2].check(rows[0].total_row_count)) ? rows[0].total_row_count:0});
		}else {
			header.push({name:acceptHeaders[2].name, value:0});
		}
	}

	if (other.hasOwnProperty('paginations')){

		if (other.paginations.hasOwnProperty('perPage')){
			header.push({name:acceptHeaders[0].name, value:(acceptHeaders[0].check(other.paginations.perPage)) ? other.paginations.perPage:0});
		}
		
		if (other.paginations.hasOwnProperty('page')){
			header.push({name:acceptHeaders[1].name, value:(acceptHeaders[1].check(other.paginations.page)) ? other.paginations.page:0});
		}

		if ((other.paginations.hasOwnProperty('perPage')) && (hasTotal)){
			header.push({name:acceptHeaders[3].name, value:(acceptHeaders[1].check(other.paginations.page) && acceptHeaders[0].check(other.paginations.perPage)) ? Math.ceil(rows[0].total_row_count/other.paginations.perPage):0});
		}else {
			header.push({name:acceptHeaders[3].name, value:0});
		}
	}
}

var filterResponse = function (){
	if (!other.hasOwnProperty('exceptions')){other.exceptions = [];}
	other.exceptions.push('total_row_count');

	for(var i=0; i<response.data.length;i++){
		for (var j=0; j<other.exceptions.length; j++){
			if (response.data[i].hasOwnProperty(other.exceptions[j])){
				response.data[i][other.exceptions[j]] = false;
				delete response.data[i][other.exceptions[j]];
			}
		}
	}
}

/* */

module.exports.set = function (rows_, other_){
	InitVars();
	response = {};
	rows = rows_ || null;
	if (!helpFunctions.isArray(rows)) if (rows === Object(rows)) rows = [rows];

	other = other_ || {};
	createResponse();

	if (!error){
		createHeaders();
		filterResponse();
	}else{
		header.push({name:acceptHeaders[2].name, value:0});
	}
}

module.exports.json = function (rows_, other_){
	if (rows_){
		module.exports.set(rows_, other_ || {});
	}
	return JSON.stringify(response);
}

module.exports.Header = function (){
	return header;
}