
/* TODO
	
	Kada postoji mysql as više tablica postoje problemi!
	Vise stupaca sa istim nazivom, nemoze se tocno odrediti sto je sto npr kad zelimo fields[]
	ili opcenito u responsu
*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';

var validator = require('validator');
var log = require(rootConfig + 'log');
var helpFunctions = require(rootLibraries + 'helpFunctions');

var safeParams = [
	'fields',
	'exclude',
	'include',
	'_order',
	'date_to',
	'date_from'
];
var acceptHeaders = [
	{name:'x-pagination-per-page',optionName:'perPage',check:function(value){return validator.isInt(value,{min:1,max:10000});}},
	{name:'x-pagination-current-page',optionName:'page',check:function(value){return validator.isInt(value,{min:1,max:2000});}}
]

var r;
var query;
var headers;
var mysql = {}
/* Private functions */
DefaulMySQLParams = function (){
	mysql = {
		columns: ['*'],
		columnSql: '',
		tablesSql: '',
		params: {
			values: [],
			keys: [],
			questionMarks: [],
			include: {},
			exclude: {},
			sql: ''
		},
		options: {  /* These are input params and are used as such */
			method:'GET',
			page:1,
			perPage:50,
			countColumn: '', /* Column which you want to count. if empty no total count will be queried */
			sumColumns: [], /* Columns for which you want to sum values */
			paginationSql:'',
			tables: [], /*  Which tables to include in mysql query {name:name,as:as_name(default false)} */ 
			columns: [], /* Only these columns can be used in query. Other columns which are requested through API param 'fields[]' are going to be ignored!! */
			initColumns: ['*'], /* If you don't want default value to be * you can change this */
			addsQuery: '', /* is adding to query at the end */
			addsColumns: '', /* is adding to columns at the end */
			fulltextColumns: [], /* perform fulltext search for this columns */
			liketextColumns: [], /* perform % like % search for this columns, don't user '=' */
			dateColumn: ''
		}
		
	}
}
CheckDefault = function (defaultOptions){
	for (var key in defaultOptions) {
	  if (defaultOptions.hasOwnProperty(key)) {
	  	mysql.options[key] = defaultOptions[key];
	  }
	}

	if (helpFunctions.isArray(mysql.options.initColumns)){
		if (mysql.options.initColumns.length > 0){
			mysql.columns = mysql.options.initColumns;
		}
	}
}

CreateParamSqlArrayFromProperty = function (property, method){
	var arr = [];
	if (query.hasOwnProperty(property) || !property){
		var obj = (query.hasOwnProperty(property))? query[property]:query;
		log.put("Query loop...")
		for (var key in obj){
			log.put("PARAMS:::"+key);
			if (safeParams.indexOf(key) >=  0) continue;
			log.put("PARAMS:::PASSED1:::"+key);
			if (mysql.options.columns.length>0)	if (mysql.options.columns.indexOf(key) !=  0) continue;
			log.put("PARAMS:::PASSED2:::"+key);
			if (SanitizeString(key, {type:'column'}) != key) continue;

			key = SanitizeString(key, {type:'column'});
			mysql.params.keys.push(key);

			log.put("PARAMS:::PASSED:::"+key);

			if(helpFunctions.isArray(obj[key])){
				var params = [];

				for (var key_ in obj[key]){
					mysql.params.values.push(obj[key][key_]);
					mysql.params.questionMarks.push('?');
					params.push('?');
				}

				if (mysql.options.liketextColumns.indexOf(key) >= 0){
					if (method == '=') //arr.push(key + ' REGEXP ' + params.join('|'));
					arr.push('('+key + ' like '+params.join(' OR ' + key + ' like ')+')')
					else arr.push('!(' + key + ' in ' + '(' + params.join(',') + '))');
				}else {
					if (method == '=') arr.push(key + ' in ' + '(' + params.join(',') + ')');
					else arr.push('!(' + key + ' in ' + '(' + params.join(',') + '))');
				}
				

			}else{ 
				//obj[key] = SanitizeString(obj[key]);

				mysql.params.values.push(obj[key]);
				mysql.params.questionMarks.push('?');
				if (key == '_kw') {
					if (mysql.options.fulltextColumns.length > 0)
						arr.push("MATCH (" + mysql.options.fulltextColumns.join(',') + ") AGAINST(?)")
				}
				else if (mysql.options.liketextColumns.indexOf(key) >= 0){
					/*mysql.params.values.pop()
					mysql.params.values.push('%'+obj[key]+'%');*/
					arr.push(key + " like ?");
				}
				else arr.push(key + method + '?');
			}

		}

	}

	return arr;
}

CreateDateRangeSqlArray = function (){
	var dateRange = [];
	if (mysql.options.dateColumn.length>0){
		if (query.hasOwnProperty('date_from')){
			if (validator.isDate(query.date_from)){
				dateRange.push(mysql.options.dateColumn + ' >= ?');
				mysql.params.values.push(query.date_from);
				mysql.params.questionMarks.push('?');
			}
		}

		if (query.hasOwnProperty('date_to')){
			if (validator.isDate(query.date_to)){
				dateRange.push(mysql.options.dateColumn + ' <= ?');
				mysql.params.values.push(query.date_to);
				mysql.params.questionMarks.push('?');
			}
		}
	}
	
	return dateRange;
}

CreateParamSql =  function (){
	var paramSql = '';
	var params = {};
	var tablesSql = '';
	var addsSql = '';

	params.include = CreateParamSqlArrayFromProperty((query.hasOwnProperty('include'))?'include':false, '=') || [];
	params.exclude = CreateParamSqlArrayFromProperty('exclude', '!=') || [];
	params.dateRange = CreateDateRangeSqlArray() || [];

	if (params.include.length>0 || params.exclude.length>0 || params.dateRange.length>0){
		paramSql = 'WHERE ';

		if (params.include.length>0){
			paramSql += '(' + params.include.join(" AND ") + ')';
		}

		if (params.dateRange.length>0){
			if (params.include.length>0) paramSql += ' AND ';
			paramSql += '(' + params.dateRange.join(" AND ") + ')';
		}

		if (params.exclude.length>0){
			if ((params.include.length>0) || (params.dateRange.length>0)) paramSql += ' AND ';
			paramSql += '(' + params.exclude.join(" AND ") + ')';
		}

	}

	if (mysql.options.addsQuery.length > 0){
		if (paramSql.length == 0) paramSql = 'WHERE '+ mysql.options.addsQuery;
		else paramSql += ' AND ' + mysql.options.addsQuery;
	}

	mysql.params.sql = paramSql;

}

CreateColumnSql =  function (){
	var columnSql = '';
	var tmpFields = [];
	var sumColumnsSql = [];

	if (query.hasOwnProperty('fields')){
		for (var key in query.fields){
			if (SanitizeString(query.fields[key]) != query.fields[key]) continue;

			tmpFields.push(query.fields[key]);
		}
		query.fields[key] = tmpFields;
		mysql.columns = query.fields; 
		/* TODO Treba napraviti posebnu knjižnicu koja ce preimenovati potrebne stupce iz baze u odgovarajuće tako da se svugdje koristi ista terminologija */
	}

	for (var key in mysql.options.sumColumns){
		//sumColumnsSql.push('SUM('+mysql.options.sumColumns[key].name+') as' + mysql.options.sumColumns[key].as);
		/* TODO SUM COLUMNS!!!! */
		mysql.columns.push('(SELECT SUM('+mysql.options.sumColumns[key].name+') FROM ' + module.exports.getTables() + ' ' + module.exports.getQueryParams() +') as ' + mysql.options.sumColumns[key].as)
		Array.prototype.push.apply(mysql.params.values, mysql.params.values);
	}

	if (mysql.options.countColumn.length > 0){
			mysql.columns.push('(SELECT count('+mysql.options.countColumn+') FROM ' + module.exports.getTables() + ' ' + module.exports.getQueryParams() +') as total_row_count')
			Array.prototype.push.apply(mysql.params.values, mysql.params.values);
		}

	mysql.columnSql = mysql.columns.join(',');

	if (mysql.options.addsColumns.length > 0){
		mysql.columnSql += ','+ mysql.options.addsColumns;
	}

	//console.log('OOVO JE', mysql.columnSql)

}

CreateTablesSql = function (){
	var array = [];
	for (var i=0; i < mysql.options.tables.length;i++){
		array.push(mysql.options.tables[i].name + ((mysql.options.tables[i].hasOwnProperty('as'))? (' as ' +  mysql.options.tables[i].as):('')));
	}

	mysql.tablesSql = array.join(",");
}

CreatePaginationSql =  function (){
	var headers_ = {};
	for (var i=0;i<acceptHeaders.length;i++){
		var key = acceptHeaders[i].name;
		headers_[key] = mysql.options[acceptHeaders[i].optionName];
		if (headers){
			if (headers.hasOwnProperty(key)){
				if (acceptHeaders[i].check(headers[key])){
					headers_[key] = headers[key];
					if (mysql.options.hasOwnProperty(acceptHeaders[i].optionName)){
						mysql.options[acceptHeaders[i].optionName] = headers[key];
					}
				}
			}
		}
	}
	headers = headers_;

	console.log('HEADERS', headers)


	var from = ((headers['x-pagination-current-page']-1) * headers['x-pagination-per-page']);
	mysql.options.paginationSql = from + ',' + headers['x-pagination-per-page'];
}

CreateOrderBySql = function (){
var sql = {
	bool: false,
	orders: []
}

	if (query.hasOwnProperty('_order')){
		for (var key in query['_order']){
			if (query['_order'][key]!='DESC' && query['_order'][key]!='desc' && query['_order'][key]!='ASC' && query['_order'][key]!='asc') continue;
			if (key == SanitizeString(key, {type:'column'}) && query['_order'][key] == SanitizeString(query['_order'][key], {type:'column'})){
				query['_order'][key] = SanitizeString(query['_order'][key],{type:'column'});
				key = SanitizeString(key,{type:'column'});

				sql.bool = true;
				sql.orders.push(key + ' ' + query['_order'][key]);
			}	
		}
	}

	if (sql.orders.length > 0){
		mysql.params.sql += ' ORDER BY ' +sql.orders.join(',') + ' ';
	}
}

/* Sanitize text */
SanitizeString = function (string, options){

	var s = string;
	var options_ = options || {};
	var opt = {
		type:'value'
	}

	if (options_.hasOwnProperty('type')) opt.type = options_.type;

	switch(opt.type){
		case 'column':
			s = s.replace(/ /g,'');
			break;
	}

	s = validator.escape(s);
	console.log(s, options);
	return s;
}


/* */

module.exports.set = function (req, defaultOptions){
	r = req;
	query = r.body;
	var count = 0;
	for (var key in query){
		count ++ ;
	}
	if (count == 0) {
		query = r.query;
	}
	console.log("This is query- > ", query)
	headers = r.headers;

	DefaulMySQLParams();

	CheckDefault(defaultOptions);

	switch(mysql.options.method){

		case 'GET':

			break;

		case 'POST':

			mysql.columnSql = '';
			break;

		case 'DELETE':

			break;

		case 'PUT':
		case 'PATCH':
		case 'UPDATE':

			mysql.columnSql = '';
			break;
	}

	CreatePaginationSql();

	CreateParamSql();

	CreateTablesSql();

	CreateColumnSql();

	CreateOrderBySql();

}

/* getParams returns array of parrams used in query */
module.exports.getParams = function (){
	return mysql.params.values;
}

module.exports.getParamKeys = function (){
	return mysql.params.keys;
}

module.exports.getQuestionMarks =  function (){
	return mysql.params.questionMarks;
}

module.exports.getQueryParams = function (){
	return mysql.params.sql || '';
}

/* getColumns returns String of columns which you want to get from mysql */
module.exports.getColumns = function (){
	return mysql.columnSql || '';
}

module.exports.getTables = function (){
	return mysql.tablesSql || '';
}

module.exports.getLimit = function (){
	return mysql.options.paginationSql;
}

module.exports.getPaginations = function (){
	return {page: mysql.options.page, perPage: mysql.options.perPage};
}

module.exports.getHeaders = function (){
	return headers;
}