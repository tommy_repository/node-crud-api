var root = './../';
var other ={};

module.exports = function(app, oth){
	other = oth;
	other.api = '/v1';

	// apply this rule to all requests accessing any URL/URI
	app.all('*', function(req, res, next) {
	  	res.header('Access-Control-Allow-Origin', req.headers.origin);
	  	res.header('Access-Control-Allow-Credentials', true);
	  	res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
	  	res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, x-accesstoken, X-Pagination-Current-Page,X-Pagination-Page-Count,X-Pagination-Per-Page,X-Pagination-Total-Count');
	  	res.header('access-control-expose-headers', 'X-Pagination-Current-Page,X-Pagination-Page-Count,X-Pagination-Per-Page,X-Pagination-Total-Count')
	    next();
	});

	// fulfils pre-flight/promise request
	app.options('/', function(req, res) {
	    res.sendStatus(200);
	});

	require('./routes/index')(app, other);
	require('./routes/user')(app, other);
	require('./routes/author')(app, other);
	require('./routes/partner')(app, other);
	require('./routes/country')(app, other);
	require('./routes/city')(app, other);
	require('./routes/cms_page')(app, other);
	require('./routes/service')(app, other);
	require('./routes/proforma')(app, other);
	require('./routes/auth')(app, other);

}