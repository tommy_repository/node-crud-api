/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries +'request');

var con = db.connection;
var response = [];
var defaultOptions = {
	perPage: 20
}

module.exports.list = function (req, res){

	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM user '+ request.getQueryParams() +'LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con.query(query, request.getParams(), function(err, rows, fields) {
		response = rows;
		res.setHeader('Content-Type', 'application/json');
    	res.send(JSON.stringify(response));
	});

}
