var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 20,
	table: 'service'
}


module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'service.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: options.table}
		],
		countColumn: 'id_service',
		dateColumn: 'date_to',
		fulltextColumns: ['name'],
		liketextColumns: ['id_service_category']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');
		if (err){
			log.put(err);
		}
		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

module.exports.service = function (req, res, next){
	if (!auth.Perm(req, res, 'service')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.id_service = req.params.id || req.query.id_service_type;

	var defaultOptions = {
		perPage: 1
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM  ' + options.table + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {};
		res_.setHeader('Content-Type', 'application/json');
		res_.send(response.json(rows, other));
	})
}

module.exports.categories = function (req, res, next){
	if (!auth.Perm(req, res, 'service.categories')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'service_category'}
		],
		countColumn: 'id_service_category'
		//fulltextColumns: ['user.name','user.surname','user.email']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

module.exports.statuses = function (req, res, next){
	if (!auth.Perm(req, res, 'service.statuses')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'service_status'}
		],
		countColumn: 'id_service_status'
		//fulltextColumns: ['user.name','user.surname','user.email']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

module.exports.types = function (req, res, next){
	if (!auth.Perm(req, res, 'service.types')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'service_type'}
		],
		countColumn: 'id_service_type'
		//fulltextColumns: ['user.name','user.surname','user.email']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

/* POST DELETE PUT */

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'service.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name: options.table} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {};
		if (err){
			other = {err: err}
		}
		
		response.set(rows, other);
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json());
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'service.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name: options.table} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE id_service=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		var other = {};
		if (err){
			other = {err: err}
			console.log('UPDATE', err)
		}
		
		response.set(rows, other);
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json());
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'service.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:options.table} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE id_service=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		var other = {};
		if (err){
			other = {err: err}
		}
		
		response.set(rows, other);
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json());
	})
}