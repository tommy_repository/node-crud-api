/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 50
}

module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'partner.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'partner'}
		],
		countColumn: 'id_partner',
		fulltextColumns: ['name','mail_public','url']
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	log.put(req.headers);
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['user.password', 'password', 'total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});

}

module.exports.partner = function (req, res, next){
	if (!auth.Perm(req, res, 'partner')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.id_partner = req.params.id || req.query.id_partner;
	console.log(req.query);

	var defaultOptions = {
		perPage: 1,
		tables: [
 			{name: 'partner'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {exceptions: ['password']}
		res_.setHeader('Content-Type', 'application/json');
		res_.send(response.json(rows, other));
	})
}

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'partner.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name:'partner'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'partner.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:'partner'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE id_partner=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){

		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'partner.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:'partner'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE id_partner=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}
