/* users controller */
/*
list

*/

var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 20
}

module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'author.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'authors'}
		],
		countColumn: 'id',
		fulltextColumns: ['name','workplace']
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		log.put(err)
		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});

}

module.exports.author = function (req, res, next){
	if (!auth.Perm(req, res, 'author')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query['id'] = req.params.id || req.query.id;
	console.log(req.query);

	var defaultOptions = {
		perPage: 1,
		tables: [
 			{name: 'authors'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res_.setHeader('Content-Type', 'application/json');
		res_.send(response.json(rows));
	})
}

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'author.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name:'authors'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'author.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:'authors'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE id=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){

		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'author.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:'authors'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE id=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}