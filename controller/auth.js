var LocalStrategy    = require('passport-local').Strategy;
var passport 	   = require('passport');
var crypto = require('crypto');

var rootConfig = '../config/';
var rootLibraries = '../libraries/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');

var connection = db.connection();
var prefix;

var perms = [
{name: 'user', auth: true, roles:[1]},
{name: 'user.list', auth: true, roles:[1]},
{name: 'user.add', auth: true, roles:[1]},
{name: 'user.update', auth: true, roles:[1]},
{name: 'user.delete', auth: true, roles:[1]},
{name: 'user.roles', auth: true, roles:[1]},
{name: 'author', auth: true, roles:[1]},
{name: 'author.list', auth: false, roles:[1]},
{name: 'author.add', auth: true, roles:[1]},
{name: 'author.update', auth: true, roles:[1]},
{name: 'author.delete', auth: true, roles:[1]},
{name: 'partner', auth: true, roles:[1]},
{name: 'partner.list', auth: false, roles:[1]},
{name: 'partner.add', auth: true, roles:[1]},
{name: 'partner.update', auth: true, roles:[1]},
{name: 'partner.delete', auth: true, roles:[1]},
{name: 'service', auth: true, roles:[1]},
{name: 'service.list', auth: false, roles:[1]},
{name: 'service.add', auth: true, roles:[1]},
{name: 'service.update', auth: true, roles:[1]},
{name: 'service.delete', auth: true, roles:[1]},
{name: 'service.categories', auth: true, roles:[1]},
{name: 'service.statuses', auth: true, roles:[1]},
{name: 'service.types', auth: true, roles:[1]},
{name: 'proforma', auth: true, roles:[1]},
{name: 'proforma.list', auth: false, roles:[1]},
{name: 'proforma.add', auth: true, roles:[1]},
{name: 'proforma.updateStatus', auth: true, roles:[1]},
{name: 'city', auth: true, roles:[1]},
{name: 'city.list', auth: false, roles:[1]},
{name: 'city.add', auth: true, roles:[1]},
{name: 'city.update', auth: true, roles:[1]},
{name: 'city.delete', auth: true, roles:[1]},
{name: 'country', auth: true, roles:[1]},
{name: 'country.list', auth: false, roles:[1]},
{name: 'country.add', auth: true, roles:[1]},
{name: 'country.update', auth: true, roles:[1]},
{name: 'country.delete', auth: true, roles:[1]}
]

/* Private functions */

function md5(string) {
  return crypto.createHash('md5').update(string || '').digest('hex');
}

/* */

module.exports.Init = function (defaultOptions){
	var options = defaultOptions || {};

	if (options.hasOwnProperty('prefix')){
		prefix = options.prefix || '';
	}

	passport.serializeUser(function(user, done) {
	  done(null, user);
	});
 
	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});

	module.exports.PassportUse();
}

module.exports.PassportUse = function (){

	passport.use('Login', new  LocalStrategy({
        passReqToCallback : true // allows us  to pass in the req from our route (lets us check if a user is logged in or  not)
    },
    function(req, username, password, done) {
	  process.nextTick(function() {
	  	var req_ = {body:{
	  		email:req.body.username || req.body.email || req.query.username,
	  		password: md5(req.body.password || req.query.password)
	  	}}
	  	request.set(req_, {method:'LOGIN'});
	  	//request.set(req_);
		var query = "SELECT " + request.getColumns() + " from user " + request.getQueryParams() + " LIMIT 1;";
		
		connection = db.connection();
		connection.query(query, request.getParams(), function (err, user, fields){
			if (!err){
				console.log(user.length + 'KOrisnik je loginar sa:: ' , user[0])

				if (user.length > 0) {
					console.log('USER', user);
					req.session.user = user[0];
					return done(null, user[0]);
				}
				else return done(null, false);
			}else return done(null,false);
		});
		
	  });
	}));

}

module.exports.logout = function (req, res){
	req.session.destroy(function (err) {
		 res.send({error:200, message:"Logged out!"});
	});

}

module.exports.isLoggedIn = function (req, res) {
    if (req.isAuthenticated())
    return true;

	return false;

    //res.redirect('/');

}

module.exports.Perm = function (req, res, type) {
	var perm = true;
	console.log('CHECK ' + type, req.user)
	for (var i=0; i<perms.length; i++){
		if (perms[i].name == type) {
			if (perms[i].auth){
				if (req.user) {
					if (perms[i].roles.indexOf(req.user.role) >=  0) perm = true;
					else perm = false;
				}
				else perm = false;
			}
		}
	}
	if (!perm){
		objSend = {error:401, message:"notLogged in"}
		res.send(JSON.stringify(objSend));
	}

	return perm;
}

module.exports.success = function (req, res){
	res.send(response.json([req.user]));
}

module.exports.failure = function (req, res){
	res.send(response.json((req.user)?[req.user]:[]));
}