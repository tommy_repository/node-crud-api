/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 100
}

module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'cms_page.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'page'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});

}

module.exports.cms_page = function (req, res, next){
	if (!auth.Perm(req, res, 'cms_page')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.id = req.params.id || req.query.id;
	console.log(req.query);

	var defaultOptions = {
		perPage: 1,
		tables: [
 			{name: 'page'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	})
}

module.exports.updateOrder = function (req, res, next){
	if (!auth.Perm(req, res, 'cms.updateOrder')) return next();

	req.session.touch();

	var pages = req.query.pages;
	var queries = {
		success:0,
		count:0,

	}

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name: 'page'} ],
		columns: ['order_id']
	}
	request.set(req, defaultOptions);
	for (var i=0;i<pages.length;i++){
		if (!pages[i]) continue;

		var query = 'UPDATE page SET order_id=? WHERE id_page=?;'; 
		var params = [i,pages[i]];

		log.put(query);
		log.put(params);

		con = db.connection();
		con.query(query, params, function (err, rows, fields){
			queries.count++;
		var other = {};

		if (err){
			console.log('UPDATE', err)
			other = {err: err}
			response.set(rows, other);
			res.send(response.json([query,request.getParams()]));
			return ;		
		}else{
			queries.success++;
		}
		
		console.log('COUNT', queries.count, pages.length, queries.success)
			if (queries.count == pages.length){
				
				if (queries.count == queries.success){
					response.set(rows, other);
				}else {
					other = {err: {err:'Failed to update all'}};
					response.set(null, other);
				}

				res.setHeader('Content-Type', 'application/json');
				res.send(response.json([query,request.getParams()]));
			}
		})
	}
}
