/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 20
}

module.exports.logged = function (req, res, next){
	if (!auth.Perm(req, res, 'user.list')) return next();

	res.send(req.user);
}

module.exports.list = function (req, res, next){
	//console.log('USSSERR', req.user);
	if (!auth.Perm(req, res, 'user.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'user'},
 			{name: 'user_roles', as:'Role'}
		],
		countColumn: 'user.id_user',
		addsQuery: 'user.role=Role.id',
		addsColumns: 'Role.id as user_roles_id',
		fulltextColumns: ['user.name','user.surname','user.email']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['user.password', 'password'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');
		log.put(request.getPaginations())
		if (err){
			log.put(err);
		}
		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});

}

module.exports.user = function (req, res, next){
	if (!auth.Perm(req, res, 'user')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.id_user = req.params.id || req.query.id_user;
	
	var defaultOptions = {
		perPage: 1
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM user ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {exceptions: ['password']}
		res_.setHeader('Content-Type', 'application/json');
		res_.send(response.json(rows, other));
	})
}

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'user.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name:'user'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'user.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:'user'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE id_user=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		//console.log('ERR', err)
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'user.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:'user'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE id_user=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.roles = function (req, res, next){
	if (!auth.Perm(req, res, 'user.delete')) return next();

	req.session.touch();

	var defaultOptions = {
		perPage: 1
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM user_roles ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}
