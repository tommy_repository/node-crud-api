/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 100
}

module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'country.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'countries'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['user.password', 'password']}
		res.setHeader('Content-Type', 'application/json');
    	res.send(response.json(rows, other));
	});

}

module.exports.country = function (req, res, next){
	if (!auth.Perm(req, res, 'country')) return next();

	var res_ = res;
	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.country_code = req.params.id || req.query.id;
	console.log(req.query);

	var defaultOptions = {
		perPage: 1,
		tables: [
 			{name: 'countries'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {exceptions: ['password']}
		res_.setHeader('Content-Type', 'application/json');
		res_.send(response.json(rows, other));
	})
}

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'country.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name:'countries'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'country.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:'countries'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE country_code=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){

		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'country.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:'countries'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE country_code=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}