var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var moment = require('moment');
var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 20,
	table: 'proforma',
	serviceTable: 'service'
}

module.exports.list = function (req, res, next){

	if (!auth.Perm(req, res, 'proforma.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: options.table},
 			{name: 'paytime'},
 			{name: options.serviceTable}
		],
		countColumn: 'proforma.id_proforma',
		sumColumns: [{name:options.serviceTable+'.provision',as:'provision_total'}, {name:'paytime.total',as: 'total_total'}],
		dateColumn: 'proforma.pay_datetime',
		initColumns: ['proforma.*'],
		addsQuery: 'proforma.id_paytime=paytime.id_paytime AND '+options.serviceTable+'.id_service = proforma.id_service',
		addsColumns: 'paytime.email as paytime_email, paytime.total as paytime_total, '+options.serviceTable+'.name as service_name, paytime.status as paytime_status, paytime.insert_datetime as paytime_insert_datetime, paytime.update_datetime as paytime_update_datetime',
		fulltextColumns: [options.serviceTable + '.name']
	}
	request.set(req, defaultOptions);

	//var queryAdd = '(SELECT count(user.id_user) FROM ' + request.getTables() + ' ' + request.getQueryParams() +') as total'

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');
		if (err){
			other.err= err;
			log.put("ERROR" +err);
		}

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

module.exports.updateStatus = function (req, res, next){
	if (!auth.Perm(req, res, 'proforma.updateStatus')) return next();

	req.session.touch();

	var id = req.params.id;
	var status = req.params.status;

	var now = moment().format('YYYY-MM-DD HH:mm:ss');
	//now.add(2, 'hours');

	var proforma_keys = [];
	var proforma_params = [];

	var paytime_keys = [];
	var paytime_params = [];

	whereAdd='';

	switch(status){
		case '3':
			proforma_keys.push('id_proforma_status=3');
			proforma_keys.push('phistory=CONCAT(?, phistory)');
			proforma_params.push(now + ' markpayed by admin_id:' + req.user.id_user);

			/* U BAZI DEFINIRANO
			proforma_keys.push('update_datetime=?');
			proforma_params.push(now);

			proforma_keys.push('pay_datetime=?');
			proforma_params.push(now);*/

			paytime_keys.push('pay.history=CONCAT(?, pay.history)');
			paytime_params.push(now + ' markpayed by admin_id:' + req.user.id_user);

			/*NE DIRAJ ZBOG TRIGGERA U BAZI 
			paytime_keys.push('pay.status=?');
			paytime_params.push(1);*/

			/* U BAZI DEFINIRANO
			paytime_keys.push('pay.update_datetime=?');
			paytime_params.push(now);*/

			//whereAdd = ' id_proforma_status !=3 AND ';
			break;
		case '4':
			proforma_keys.push('id_proforma_status=4');
			/*paytime_keys.push('pay.update_datetime=?');
			paytime_params.push(now);*/

			paytime_keys.push('pay.history=CONCAT(?, pay.history)');
			paytime_params.push(now + ' markstorno by admin_id:' + req.user.id_user);

			paytime_keys.push('pay.status=?');
			paytime_params.push(0);
			/*proforma_keys.push('update_datetime=?');
			proforma_params.push(now);*/

			proforma_keys.push('phistory=CONCAT(?, phistory)');
			proforma_params.push(now + ' markstorno by admin_id:' + req.user.id_user);

			//whereAdd = ' id_proforma_status !=4 AND ';
			break;
	}
	
	var query = 'UPDATE proforma SET ' + proforma_keys.join(",") + ' WHERE ' + whereAdd + 'id_paytime=' + id +';'; 

	log.put(query);
	log.put(proforma_params);

	con = db.connection();
	con.query(query, proforma_params, function (err, rows, fields){
		var other = {};
		if (err){
			other = {err: err}
			response.set(rows, other);
			res.setHeader('Content-Type', 'application/json');
			res.send(response.json());
		}else{
			var query2 = 'UPDATE paytime pay SET ' + paytime_keys.join(",") + ' WHERE pay.id_paytime=' + id +';'; 

			log.put(query2);
			log.put(paytime_params);

			var con2 = db.connection();
			con2.query(query2, paytime_params, function (err, rows, fields){
				var other = {};
				if (err){
					log.put(err)
					other = {err: err}
				}
				
				response.set(rows, other);
				res.setHeader('Content-Type', 'application/json');
				res.send(response.json());
			})
		}
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'proforma.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:options.table} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE id_proforma=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		var other = {};
		if (err){
			other = {err: err}
		}
		
		response.set(rows, other);
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json());
	})
}

module.exports.statuses = function (req, res, next){

	if (!auth.Perm(req, res, 'proforma.statuses')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'proforma_status'}
		]
		//fulltextColumns: ['name']
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');
		if (err){
			other.err= err;
			log.put("ERROR" +err);
		}

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}

module.exports.payments = function (req, res, next){

	if (!auth.Perm(req, res, 'proforma.payments')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'paytime_payment'}
		]
		//fulltextColumns: ['name']
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {
		var other = {exceptions: ['total'], paginations: request.getPaginations()}
		//res.set('Access-Control-Allow-Origin', '*');
		if (err){
			other.err= err;
			log.put("ERROR" +err);
		}

		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});
}