/* users controller */
/*
list

*/
var rootConfig = '../config/';
var rootLibraries = '../libraries/';
var rootControllers = '../controller/';

var log = require(rootConfig + 'log');
var db = require(rootConfig + 'db');
var request = require(rootLibraries + 'request');
var response = require(rootLibraries + 'response');
var auth = require(rootControllers + 'auth');

var con = db.connection();

var options = {
	perPage: 100
}

module.exports.list = function (req, res, next){
	if (!auth.Perm(req, res, 'city.list')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	var defaultOptions = {
		perPage: options.perPage,
		tables: [
 			{name: 'cities'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT '+ request.getColumns() +' FROM ' + request.getTables() + ' ' + request.getQueryParams() +' LIMIT ' + request.getLimit() + ';';
	log.put(query);
	log.put(request.getParams());
	
	con = db.connection();
	con.query(query, request.getParams(), function(err, rows, fields) {

    	var other = {exceptions: ['user.password', 'password'], paginations: request.getPaginations()}
		log.put(err)
		response.set(rows, other);
		var h = response.Header();
		for (var i=0;i<h.length;i++){
			res.setHeader(h[i].name, h[i].value);
		}

    	res.send(response.json());
	});

}

module.exports.city = function (req, res, next){
	console.log("REQUEST WTF", req.user)
	if (!auth.Perm(req, res, 'city')) return next();

	req.session.touch(); //is called to reset req.session.maxAge to its original value

	req.query.city_id = req.params.id || req.query.id;

	var defaultOptions = {
		perPage: 1,
		tables: [
 			{name: 'cities'}
		]
	}
	request.set(req, defaultOptions);

	var query = 'SELECT ' + request.getColumns() + ' FROM ' + request.getTables() + ' ' + request.getQueryParams() + ' LIMIT ' + request.getLimit() + ';';

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		var other = {exceptions: ['password']}
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows, other));
	})
}

module.exports.add = function (req, res, next){
	if (!auth.Perm(req, res, 'city.add')) return next();

	req.session.touch();

	var defaultOptions = {
		method: 'POST',
		tables: [ {name:'cities'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'INSERT INTO ' + request.getTables() + ' (' + request.getParamKeys().join(",") + ') VALUES(' + request.getQuestionMarks().join(",") +');';
	
	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, request.getParams(), function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.update = function (req, res, next){
	if (!auth.Perm(req, res, 'city.update')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'UPDATE',
		tables: [ {name:'cities'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'UPDATE ' + request.getTables() + ' SET ' + request.getParamKeys().join("=?,") + '=? WHERE city_id=?;'; 
	var params = request.getParams();
	params.push(id);

	log.put(query);
	log.put(request.getParams());

	con = db.connection();
	con.query(query, params, function (err, rows, fields){

		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

module.exports.delete = function (req, res, next){
	if (!auth.Perm(req, res, 'city.delete')) return next();

	req.session.touch();

	var id = req.params.id;

	var defaultOptions = {
		method: 'DELETE',
		tables: [ {name:'cities'} ]
	}
	request.set(req, defaultOptions);
	
	var query = 'DELETE FROM ' + request.getTables() + ' WHERE city_id=?;';
	var params = [];
	params.push(id);

	log.put(query);
	log.put(params);

	con = db.connection();
	con.query(query, params, function (err, rows, fields){
		res.setHeader('Content-Type', 'application/json');
		res.send(response.json(rows));
	})
}

