module.exports = function (app, other){
	var user = require('../controller/user.js');
	var prefix = other.api + '/user';

	user.passport = other.passport;
	/* GET */
	app.get(prefix + '/logged', user.logged)
	app.get(prefix + '/id/:id/', user.user);
	app.get(prefix + '/list/', user.list);
	app.get(prefix + '/roles/', user.roles);
	/* */

	/* POST new */
	app.post(prefix + '/', user.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', user.update);
	app.patch(prefix + '/id/:id', user.update);
	/* */

	/* DELETE */
	//app.delete(prefix + '/id/:id', user.delete);
	/* */
}