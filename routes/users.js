var prefix = '/users';
var users = require('../controller/users.js');

module.exports = function (app, root){
	/* GET */
	app.get(prefix + '/', users.list);
	app.get(prefix, users.list);
	/* */
}