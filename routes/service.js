module.exports =  function (app, other){
	var service = require('../controller/service.js');
	var prefix = other.api + '/service';
	service.passport = other.passport;

	/* GET */
	app.get(prefix + '/list', service.list);
	app.get(prefix + '/id/:id', service.service);
	app.get(prefix + '/categories', service.categories);
	app.get(prefix + '/statuses', service.statuses);
	app.get(prefix + '/types', service.types);
	/* */

	/* POST new */
	app.post(prefix + '/', service.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', service.update);
	app.patch(prefix + '/id/:id', service.update);
	/* */

	/* DELETE */
	app.delete(prefix + '/id/:id', service.delete);
	/* */
}