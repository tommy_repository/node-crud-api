module.exports = function (app, other){
	var cms_page = require('../controller/cms_page.js');
	var prefix = other.api + '/cms_page';

	cms_page.passport = other.passport;
	/* GET */
	app.get(prefix + '/id/:id/', cms_page.cms_page);
	app.get(prefix + '/list/', cms_page.list);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/updateOrder/', cms_page.updateOrder)
	/* */
}