module.exports = function (app, other){
	var prefix = other.api + '/auth';
	var auth =  require('../controller/auth');

	auth.Init({
		prefix:prefix
	});

	/*app.post(prefix + '/login', other.passport.authenticate('Login', {
	    successRedirect: prefix + '/loginSuccess',
	    failureRedirect: prefix + '/loginFailure'
	  }))*/

	app.post(prefix + '/login', function(req, res, next) {
		console.log('LAST', req.session.last);
	  other.passport.authenticate('Login', {session: true},  function(err, user, info) {

	  	if (err) { auth.failure(req,res); return next();}
	  	else if (!user) { auth.failure(req,res); return next(); }


	  	req.login(user, function(err) {
	    	if (err) { auth.failure(req,res); return next(); }
	    	req.session.user = req.user;
	    	console.log('KORISNIK    aaa ', req.user)
	      	auth.success(req,res);
	    });

	  })(req, res, next);
	});

	app.post(prefix + '/logout', auth.logout);
}