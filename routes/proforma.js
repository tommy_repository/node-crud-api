module.exports =  function (app, other){
	var proforma = require('../controller/proforma.js');
	var prefix = other.api + '/proforma';
	proforma.passport = other.passport;

	/* GET */
	app.get(prefix + '/list', proforma.list);
	app.get(prefix + '/statuses', proforma.statuses);
	app.get(prefix + '/payments', proforma.payments);
	/*      */

	/* POST */
	app.post(prefix + '/id/:id/status/:status', proforma.updateStatus)
	/*     */
}