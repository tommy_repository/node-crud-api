module.exports = function (app, other){
	var author = require('../controller/author.js');
	var prefix = other.api + '/author';

	author.passport = other.passport;
	/* GET */
	app.get(prefix + '/id/:id/', author.author);
	app.get(prefix + '/list/', author.list);
	/* */

	/* POST new */
	app.post(prefix + '/', author.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', author.update);
	app.patch(prefix + '/id/:id', author.update);
	/* */

	/* DELETE */
	app.delete(prefix + '/id/:id', author.delete);
	/* */
}