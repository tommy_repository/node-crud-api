module.exports = function (app, other){
	var partner = require('../controller/partner.js');
	var prefix = other.api + '/partner';

	partner.passport = other.passport;
	/* GET */
	app.get(prefix + '/id/:id/', partner.partner);
	app.get(prefix + '/list/', partner.list);
	/* */

	/* POST new */
	app.post(prefix + '/', partner.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', partner.update);
	app.patch(prefix + '/id/:id', partner.update);
	/* */

	/* DELETE */
	app.delete(prefix + '/id/:id', partner.delete);
	/* */
}