module.exports = function (app, other){
	var city = require('../controller/city.js');
	var prefix = other.api + '/city';

	city.passport = other.passport;
	/* GET */
	app.get(prefix + '/id/:id/', city.city);
	app.get(prefix + '/list/', city.list);
	/* */

	/* POST new */
	app.post(prefix + '/', city.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', city.update);
	app.patch(prefix + '/id/:id', city.update);
	/* */

	/* DELETE */
	app.delete(prefix + '/id/:id', city.delete);
	/* */
}