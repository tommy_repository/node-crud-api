module.exports = function (app, other){
	var country = require('../controller/country.js');
	var prefix = other.api + '/country';

	country.passport = other.passport;
	/* GET */
	app.get(prefix + '/id/:id/', country.country);
	app.get(prefix + '/list/', country.list);
	/* */

	/* POST new */
	app.post(prefix + '/', country.add);
	/* */

	/* PUT/PATCH */
	app.put(prefix + '/id/:id', country.update);
	app.patch(prefix + '/id/:id', country.update);
	/* */

	/* DELETE */
	app.delete(prefix + '/id/:id', country.delete);
	/* */
}