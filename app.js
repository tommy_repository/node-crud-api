// modules =================================================
var express        = require('express');
var session 	   = require('express-session');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var passport 	   = require('passport');
var LocalStrategy  = require('passport-local').Strategy;
var morgan 		   = require('morgan');
var fs 		       = require('fs');
var fsRotator 	   = require('file-stream-rotator');

var port = process.env.PORT || 8000;

var logDirectory = '/home/EklinikaAdminlog' 
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// create a rotating write stream
var accessLogStream = fsRotator.getStream({
  filename: logDirectory + '/access-%DATE%.log',
  frequency: 'daily',
  verbose: false
})
morgan.token('user', function getId(req) {
	var user = 'User is not logged:: ';
	if (req.user){
		user = 'Id:' +req.user.id_user + ',Name:' +req.user.name + ',Email'+req.user.email + ':: ';
	}
  return user;
})

app.use(morgan(':user :method :url :res[content-length] :response-time ms (:date[web])', {stream: accessLogStream}));

app.use(session({
    secret: 'cookie_secret',
    name: 'EklinikaAdmin',
    cookie: { maxAge: 60*1000*60*60 },
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json()); 

//app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

app.use(bodyParser.urlencoded({ extended: true })); 

app.use(methodOverride('X-HTTP-Method-Override')); 

app.use(express.static(__dirname + '/public')); 

/*app.use(function(req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();

});*/

app.disable('etag');

require('./routes')(app, {passport:passport});


app.listen(port, function() {
  console.log('Listening on ' + port);
});

//app.listen(port);             
                
console.log('Node server started at::' + port);

exports = module.exports = app;